package tech.cajutis.qrcodebuilder.controller;

import java.util.List;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.javase.QRCode;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class Controller {
	
	@CrossOrigin
	@GetMapping("/")
	public ResponseEntity<?> helloApi() throws IOException{
		
		List<String> teste = new ArrayList<String>();
		
		File file = QRCode.from("Hello World").to(ImageType.JPG).file();
		
		file.renameTo(new File("C:\\Users\\iferreir\\Desktop\\qrcode.jpg"));

		
		
		
		return new ResponseEntity<String>("Api works!", HttpStatus.OK);
	}

}
